#include<stdio.h>

int gt(int n){
    int s=1;
    for (int i=2;i<=n;i++)
        s=s*i;
    return s;
}
int main(){
    int k,n;
    printf("Nhap n: ");
    scanf("%d",&n);
    while (n<0){
        printf("Nhap lai n: ");
        scanf("%d",&n);
    }
    printf("Nhap k: ");
    scanf("%d",&k);
    while (k<0 || (k>n)){
        printf("Nhap lai k: ");
        scanf("%d",&k);
    }
    k=gt(n)/(gt(k)*gt(n-k));
    printf("\nKet qua: %d",k);
}
