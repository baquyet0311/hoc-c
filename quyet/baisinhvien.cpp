#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <ctype.h>
#define Max 100
struct sinhvien{
	char masv[6];
	char hoten[20];
	char lop[6];
	float dtoan,dtin,tongdiem=0;
	char diachi[50];
};
typedef sinhvien sv;
void nhap(sv sv1[Max],int &n){
	printf("nhap so sinh vien: ");scanf("%d",&n);
	for(int i=1;i<=n;i++){
		fflush(stdin);
		printf("\n nhap sinh vien thu %d: ",i);
		printf("\nnhap ma sinh vien: ");gets(sv1[i].masv);
		printf("nhap ho ten sv: ");gets(sv1[i].hoten);
		printf("nhap lop: ");gets(sv1[i].lop);
		printf("dia chi: ");gets(sv1[i].diachi);
		printf("nhap diem toan: ");scanf("%f",&sv1[i].dtoan);
		printf("nhap diem tin: ");scanf("%f",&sv1[i].dtin);
		sv1[i].tongdiem=(sv1[i].dtoan+sv1[i].dtin)/2;
	}
}
void xuat(sv sv1[],int &n){
	printf("\n\t\t\t     DANH SACH SINH VIEN:\n\n");
	printf("STT\tmasv\t\t  hoten\t\t    lop\t\t  dtoan\t\t  dtin\t\tdiachi\t\t diemtb");
	printf("\n");
	for(int i=1;i<=n;i++){
		printf("%d\t%s\t\t  %s\t    %s\t\t  %g\t\t  %g\t\t%s\t\t %g",i,sv1[i].masv,sv1[i].hoten,sv1[i].lop,sv1[i].dtoan,sv1[i].dtin,sv1[i].diachi,sv1[i].tongdiem);
		printf("\n");
	}
}
void tinhtong(sv sv1[],int &n){
	printf("\n___________________________________________\n");
	for(int i=1;i<=n;i++){
		sv1[i].tongdiem=(sv1[i].dtoan+sv1[i].dtin)/2;
		printf(" diem tb cua sv thu %d la: %g",i,sv1[i].tongdiem);
		printf("\n");
	}
}
void doicho(sv &sv2,sv &sv3){
	sinhvien a;
	a=sv2;sv2=sv3;sv3=a;
}
void sapxep(sv sv1[],int &n){
	
	int i,j;
	for(i=1;i<n;i++){
		for(j=i+1;j<=n;j++){
			if(sv1[i].tongdiem<sv1[j].tongdiem){
				doicho(sv1[i],sv1[j]);
			}
		}
	}
}
int timkiem(sv sv1[],int &n){
	int x,i;
	printf("nhap so thu tu: ");scanf("%d",&x);
	for ( i = 1; i <= n; i++){
		if(i == x){
			printf("%d\t%s\t\t  %s\t    %s\t\t dtin: %g\t\t dtoan: %g\tdiachi: %s",i,sv1[i].masv,sv1[i].hoten,sv1[i].lop,sv1[i].dtoan,sv1[i].dtin,sv1[i].diachi);
			printf("\n");
		}
	}	return -1;
}
int main(){
	int n,k;
	bool danhap = false;
	sv sv1[Max];
	while(true){
		system("cls");
		printf("| MENU SINH VIEN |");
	printf("\n_________________________________\n");
	printf("1:nhap ds sinh vien\n");
	printf("2:in ds sinh vien\n");
	printf("3:diem tb sinh vien\n");
	printf("4:sap xep sinh vien\n");
	printf("5:tim kiem sinh vien\n");
	printf("0:thoat");
	printf("\n____________________\n");
	printf("moi chon: ");scanf("%d",&k);
	switch(k){
		case 1:
			printf("\nmoi nhap ds sinh vien\n");
			nhap(sv1,n);
			danhap=true;
			printf("nhap thanh cong\n");
			getch();
			break;
		case 2:
			if(danhap){
				printf("in danh sach sinh vien\n");
				xuat(sv1,n);
			}else{
				printf("can nhap ds sinh vien truoc\n");
			}
			getch();
			break;
		case 3:
			if(danhap){
				printf("diem trung binh sinh vien\n");
				tinhtong(sv1,n);
			}else{
				printf("can nhap ds sinh vien truoc \n");
			}
			getch();
			break;
		case 4:
			if(danhap){
				printf("sap xep xong\n");
				sapxep(sv1,n);
				
			}else{
				printf("can nhap ds sinh vien truoc\n");
			}
			getch();
			break;
		case 5:
			if (danhap){
				printf("sinh vien can tim la\n");
				timkiem(sv1,n);
				
			}else{
				printf("can nhap ds sinh vien truoc\n");
			}
			getch();
			break;
		case 0:
                printf("\nBan da chon thoat chuong trinh!");
                char c;
                printf("\nban co muon tiep tuc khong:y/n?");fflush(stdin);scanf("%c",&c);
                if(c=='y'){
                	k=1;
				}else{
					printf("\nbye bye");
					 getch();
                	return 0;
				}
        default:
                printf("\nKhong co chuc nang nay!");
                printf("\nBam phim bat ky de tiep tuc!\n");
                getch();
                break;
	}
	}
}
