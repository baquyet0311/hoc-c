#include <stdio.h>

int main(){
	
	int a;
	printf("Moi nhap so co 4 chu so: \n");
	scanf("%d",&a);
	
	if (a < 1000 || a > 9999) {
		printf("\n Day khong phai la so co 4 chu so");
		return false;
	}
	
	int nghin, tram, chuc, donvi;
	nghin = a/1000;
	tram = (a%1000)/100;
	chuc = ((a%1000)%100)/10;
	donvi = ((a%1000)%100)%10;
	
	switch (nghin)
	{
	    case 1:
	      printf("Mot nghin ");
	      break;
	    case 2:
	      printf("Hai nghin ");
	      break;
	    case 3:
	      printf("Ba nghin ");
	      break;
	    case 4:
	      printf("Bon nghin ");
	      break;
	    case 5:
	      printf("Nam nghin ");
	      break;
	    case 6:
	      printf("Sau nghin ");
	      break;
	    case 7:
	      printf("Bay nghin ");
	      break;
	    case 8:
	      printf("Tam nghin ");
	      break;
	    case 9:
	      printf("Chin nghin ");
	      break;
	}

	switch (tram)
	{
	    case 1:
	      printf("Mot tram ");
	      break;
	    case 2:
	      printf("Hai tram ");
	      break;
	    case 3:
	      printf("Ba tram ");
	      break;
	    case 4:
	      printf("Bon tram ");
	      break;
	    case 5:
	      printf("Nam tram ");
	      break;
	    case 6:
	      printf("Sau tram ");
	      break;
	    case 7:
	      printf("Bay tram ");
	      break;
	    case 8:
	      printf("Tam tram ");
	      break;
	    case 9:
	      printf("Chin tram ");
	      break;
	}
	
	switch (chuc)
	{
	    case 1:
	      printf("Muoi ");
	      break;
	    case 2:
	      printf("Hai Muoi ");
	      break;
	    case 3:
	      printf("Ba Muoi ");
	      break;
	    case 4:
	      printf("Bon Muoi ");
	      break;
	    case 5:
	      printf("Nam Muoi ");
	      break;
	    case 6:
	      printf("Sau Muoi ");
	      break;
	    case 7:
	      printf("Bay Muoi ");
	      break;
	    case 8:
	      printf("Tam Muoi ");
	      break;
	    case 9:
	      printf("Chin Muoi ");
	      break;
	}	
	
	switch (donvi)
	{
	    case 1:
	      printf("Mot ");
	      break;
	    case 2:
	      printf("Hai ");
	      break;
	    case 3:
	      printf("Ba ");
	      break;
	    case 4:
	      printf("Bon ");
	      break;
	    case 5:
	      printf("Nam ");
	      break;
	    case 6:
	      printf("Sau ");
	      break;
	    case 7:
	      printf("Bay ");
	      break;
	    case 8:
	      printf("Tam ");
	      break;
	    case 9:
	      printf("Chin ");
	      break;
	}	
}
