#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <ctype.h>
#define Max 100
struct banhang{
	char mahang[6];
	char tenhang[20];
	char donvi[6];
	float gia,tien;
	char nhasx[50];
};
typedef banhang bh;
void nhap(bh bh1[],int &n){
	printf("nhap so hang: ");scanf("%d",&n);
	for(int i=1;i<=n;i++){
		fflush(stdin);
		printf("\n nhap mat hang thu %d: ",i);
		printf("\nnhap ma hang: ");gets(bh1[i].mahang);
		printf("nhap ten hang: ");gets(bh1[i].tenhang);
		printf("don vi: ");gets(bh1[i].donvi);
		printf("nha san xuat: ");gets(bh1[i].nhasx);
		printf("gia ");scanf("%f",&bh1[i].gia);
}
}
void xuat(bh bh1[],int n){
	printf("\n\t\t\t     QUAN LY BAN HANG:\n\n");
	printf("STT\tmahang\t\t  tenhang\t  donvi\t\t  nha san xuat\t\t  gia");
	printf("\n");
	for(int i=1;i<=n+1;i++){
		printf("%d\t%s\t\t  %s\t\t  %s\t\t  %s\t\t\t  %g",i,bh1[i].mahang,bh1[i].tenhang,bh1[i].donvi,bh1[i].nhasx,bh1[i].gia);
		printf("\n");
	}
}
void timkiem(bh bh1[],int n){
		int x,i;
	printf("nhap so thu tu: ");scanf("%d",&x);
	for ( i = 1; i <= n; i++){
		if(i == x){
			printf("%d\t%s\t\t  %s\t\t  %s\t\t  %s\t\t\t  %g",i,bh1[i].mahang,bh1[i].tenhang,bh1[i].donvi,bh1[i].nhasx,bh1[i].gia);
			printf("\n");
		}
	}	
}
void doicho(bh &bh2,bh &bh3){
	banhang a;
	a=bh2;bh2=bh3;bh3=a;
}
void sapxep(bh bh1[],int n){
	int i,j;
	for(i=1;i<n;i++){
		for(j=i+1;j<=n;j++){
			if(bh1[i].gia>bh1[j].gia){
				doicho(bh1[i],bh1[j]);
			}
		}
	}
}
void tinhtien(bh bh1[],int n){
	int z,t,o;
	printf("nhap so thu tu: ");scanf("%d",&z);
	printf("nhap so luong: ");scanf("%d",&t);
	printf("giam bao nhieu % : ");scanf("%d",&o);
	for(int i=1;i<=n;i++){
		if(i==z){
			bh1[i].tien=t*bh1[i].gia-(bh1[i].gia*o)/100;
			printf("gia tien san pham thu %d la: %g",i,bh1[i].tien);
		}
	}
}
void xoa(bh bh1[],int n){
	int i,g;
	printf("nhap STT: ");scanf("%d",&g);
	for(i=1;i<=n;i++){
		if(i==g){
			bh1[i]={'\0'};
		}
	}
}
void bosung(bh bh1[],int n){
	int q=n+1;
		bh1[q]=bh1[n+1];
		fflush(stdin);
		printf("\n nhap mat hang thu %d: ",q);
		printf("\nnhap ma hang: ");gets(bh1[q].mahang);
		printf("nhap ten hang: ");gets(bh1[q].tenhang);
		printf("don vi: ");gets(bh1[q].donvi);
		printf("nha san xuat: ");gets(bh1[q].nhasx);
		printf("gia ");scanf("%f",&bh1[q].gia);
		n++;
}
int main(){
	int n,k;
	bool danhap = false;
	bh bh1[Max];
	while(true){
		system("cls");
		printf("| QUAN LY BAN HANG |");
	printf("\n_________________________________\n");
	printf("1:nhap mat hang\n");
	printf("2:in mat hang\n");
	printf("3:sap xep mat hang\n");
	printf("4:tim kiem mat hang\n");
	printf("5:tinh tien mat hang\n");
	printf("6:xoa mat hang\n");
	printf("7:bo sung mat hang\n");
	printf("0:thoat");
	printf("\n____________________\n");
	printf("moi chon: ");scanf("%d",&k);
	switch(k){
		case 1:
			printf("\nmoi nhap ds mat hang\n");
			nhap(bh1,n);
			danhap=true;
			printf("nhap thanh cong\n");
			getch();
			break;
		case 2:
			if(danhap){
				printf("in danh sach mat hang\n");
				xuat(bh1,n);
			}else{
				printf("can nhap ds mat hang truoc\n");
			}
			getch();
			break;
		case 3:
			if(danhap){
				printf("sap xep xong\n");
				sapxep(bh1,n);
				
			}else{
				printf("can nhap ds mat hang truoc\n");
			}
			getch();
			break;
		case 4:
			if (danhap){
				printf("mat hang can tim la\n");
				timkiem(bh1,n);
				
			}else{
				printf("can nhap ds mat hang truoc\n");
			}
			getch();
			break;
		case 5:
			if (danhap){
				printf("tinh tien mat hang\n");
				tinhtien(bh1,n);
				
			}else{
				printf("can nhap ds mat hang truoc\n");
			}
			getch();
			break;
		case 6:
			if (danhap){
				xoa(bh1,n);
				printf("da xoa");
				
			}else{
				printf("can nhap ds mat hang truoc\n");
			}
			getch();
			break;
		case 7:
			if (danhap){
				bosung(bh1,n);
				printf("da bo sung");
				
			}else{
				printf("can nhap ds mat hang truoc\n");
			}
			getch();
			break;
		case 0:
                printf("\nBan da chon thoat chuong trinh!");
                char c;
                printf("\nban co muon tiep tuc khong:y/n?");fflush(stdin);scanf("%c",&c);
                if(c=='y'){
                	k=1;
				}else{
					printf("\nbye bye");
					 getch();
                	return 0;
				}
        default:
                printf("\nKhong co chuc nang nay!");
                printf("\nBam phim bat ky de tiep tuc!\n");
                getch();
                break;
	}
	}
}
